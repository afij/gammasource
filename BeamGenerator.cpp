#include "TH1F.h"
#include "TH2F.h"
#include "TROOT.h"

#include "GammaSource.h"
#include "TTree.h"
#include "TFile.h"

#include <TStyle.h>
#include <stdlib.h>
#include <string>       
#include <iostream>     
#include <sstream> 

using namespace std;	
void MakeRootOutput(GammaSource* gammaSource, int argc, char** argv);

int main(int argc, char** argv)
{	
	
	if(argc < 4)
	{
	    cout << "\033[0;107;31m";
	    std::cout << "Gamma beam Generator requires 3 arguments: " << std::endl;
	    std::cout << "First argument is energy index taking values";
	    std::cout << "from 0 to 3 in accordance with the convention" << std::endl;
	    std::cout << "0 = 5.8 MeV, 1 = 8.3 MeV, 2 = 13 MeV, 3 = 17 MeV " << std::endl;
	    std::cout << "Second argument defines beam start position along x axis" << std::endl;
	    std::cout << "Third argument is a number of generated events";
	    std::cout << "\033[0m" << std::endl;
	    return 0;
	}
	int energyIndex = atoi(argv[1]);
	double xPos = atof(argv[2]);
    GammaSource* gammaSource = new GammaSource(energyIndex,xPos);
   
    MakeRootOutput(gammaSource, argc, argv);


    delete gammaSource;
	return 0;
}

void MakeRootOutput(GammaSource* gammaSource, int argc, char** argv)
{   
    double energy0 = gammaSource->GetMainEnergy();
    //double xPos = atof(argv[2]);
    
    std::ostringstream strs;
    strs << energy0;
    std::string energyStr = strs.str(); 
    std::string name = "out_" + energyStr +"MeV.root";
    
    std::vector<double> momentum(3);
    std::vector<double> position(3);
    double randomEn;
        
    TFile* file = new TFile(name.c_str(), "recreate");
    TTree* tree = new TTree("gammaBeam", "gammaBeam");
    
    tree->Branch("posX", &position.at(0));
    tree->Branch("posY", &position.at(1));
    tree->Branch("posZ", &position.at(2));
    tree->Branch("momentumX", &momentum.at(0));
    tree->Branch("momentumY", &momentum.at(1));
    tree->Branch("momentumZ", &momentum.at(2));
    tree->Branch("energy", &randomEn);

    int nrOfCounts = atoi(argv[3]);
    for (int i = 0; i < nrOfCounts; i++) 
    {
        gammaSource->GetRandomMomentum(momentum, position);
        randomEn = gammaSource->GetRandomEnergy();
        tree->Fill();
    }
    file->Write();
    file->Close();
}


