// $Id: GammaSource.h 16.10.2017 A Fijalkowska $
//
/// \file GammaSource.hh
/// \brief Definition of the GammaSource class
//
#ifndef GammaSource_H
#define GammaSource_H1
#include "TF1.h"
#include "TRandom.h"

class GammaSource
{
  public:

    GammaSource(int energyIndex, double x0inMM, double sourceDistanceinM = 18.5); 
    ~GammaSource();
    double GetMainEnergy()
    {
        return energy;
    }
    double GetRandomEnergy()
    {
		return energyDistr->GetRandom();
	}
    void GetRandomMomentum(std::vector<double> &momentum, std::vector<double> &position);

               
  private:
    TF1* energyDistr;
    TF1* radiusDistr;
    TRandom* angleDistr;
    void MakeEnergyAndRadiusDistr(int energyIndex);
    double sourceDistance;
    TF1* MakeFunc5dot8MeV(std::string name, double limit);
    TF1* MakeFunc8dot3MeV(std::string name, double limit);
    TF1* MakeFunc13MeV(std::string name, double limit);
    TF1* MakeFunc17MeV(std::string name, double limit);
    void MakeEnergyDistr(double energy);
    double x0;
    double energy;
};



#endif

