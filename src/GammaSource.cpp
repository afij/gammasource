#include "GammaSource.h"
#include "TMath.h"
#include "TRandom3.h"
#include <stdexcept>


GammaSource::GammaSource(int energyIndex, double x0inMM, double sourceDistanceinM)
{
	MakeEnergyAndRadiusDistr(energyIndex); 
    angleDistr = new TRandom();
    //angleDistr->SetSeed();
    sourceDistance = sourceDistanceinM;
    x0 = x0inMM;
    gRandom = new TRandom3(0);
}

GammaSource::~GammaSource()
{
	delete energyDistr;
	delete radiusDistr;
	delete angleDistr;
}

TF1* GammaSource::MakeFunc5dot8MeV(std::string name, double limit)
{
    TF1 *beamDistr 
    = new TF1(name.c_str(), 
    "(x<-[0])*([1]**(x+[2]) + [3]) + (x>[0])*([4]**(x+[5]) + [3]) +(x<[6]-[9] && x>-[6]+[9])* [7]*(cos([8]*(x+[9])) + [10]*cos([8]*[8]*(x+[9])*(x+[9])))", -limit, limit);
    
    //fist expo
    beamDistr->SetParameter(0, 2.6);
    beamDistr->SetParameter(1, 8);
    beamDistr->SetParameter(2, 6.27926);
    beamDistr->SetParameter(3, 0.9);
    
    //sec expo
    beamDistr->SetParameter(4, 0.1);
    beamDistr->SetParameter(5, -6.07758);
    
    //cos
    beamDistr->SetParameter(6, 2.55);
    beamDistr->SetParameter(7, 122369);
    beamDistr->SetParameter(8, 0.446382);    
    beamDistr->SetParameter(9, -0.0851866);
    beamDistr->SetParameter(10, 4.86784);
    
    return beamDistr;  
}

TF1* GammaSource::MakeFunc8dot3MeV(std::string name, double limit)
{
    TF1 *beamDistr 
    = new TF1(name.c_str(), 
    "(x<-[0])*([1]**(x+[2]) + [3]) + (x>[0])*([4]**(x+[5]) + [3]) +(x<[6]-[9] && x>-[6]+[9])* [7]*(cos([8]*(x+[9])) + [10]*cos([8]*[8]*(x+[9])*(x+[9])))", -limit, limit);
 
    //fist expo
    beamDistr->SetParameter(0, 2);
    beamDistr->SetParameter(1, 12.7304);
    beamDistr->SetParameter(2, 4.97811);
    beamDistr->SetParameter(3, 2.0);
    
    //sec expo
    beamDistr->SetParameter(4, 0.0879368);
    beamDistr->SetParameter(5, -4.9744);
    
    //cos
    beamDistr->SetParameter(6, 2.05213);
    beamDistr->SetParameter(7, 173798);
    beamDistr->SetParameter(8, 0.521049);   
    beamDistr->SetParameter(9, 0.0419971);
    beamDistr->SetParameter(10, 4.02907);
    
    
    return beamDistr;  
}

TF1* GammaSource::MakeFunc13MeV(std::string name, double limit)
{
    TF1 *beamDistr 
    = new TF1(name.c_str(), 
    "(x<-[0])*([1]**(x+[2]) + [3]) + (x>[0])*([4]**(x+[5]) + [3]) +(x<[6] && x>-[6])* [7]*(cos([8]*(x+[9])) + [10]*cos([8]*[8]*(x+[9])*(x+[9])))", -limit, limit);

    
    beamDistr->SetParameter(0, 1.8);
    beamDistr->SetParameter(1, 488.7);
    beamDistr->SetParameter(2, 3.13279);
    beamDistr->SetParameter(3, 1.8);
    
    beamDistr->SetParameter(4, 0.0012456);
    beamDistr->SetParameter(5, -3.07399);

    beamDistr->SetParameter(6, 1.9);
    beamDistr->SetParameter(7, 393906);
    beamDistr->SetParameter(8, 0.65958);    
    beamDistr->SetParameter(9, -0.00791267);
    beamDistr->SetParameter(10, 1.77531);
    
    return beamDistr;  
}

TF1* GammaSource::MakeFunc17MeV(std::string name, double limit)
{
    TF1 *beamDistr 
    = new TF1(name.c_str(), 
    "(x<-[0])*([1]**(x+[2]) + [3]) + (x>[0])*([4]**(x+[5]) + [3]) +(x<[6]-[9] && x>-[6]+[9])* [7]*(cos([8]*(x+[9])) + [10]*cos([8]*[8]*(x+[9])*(x+[9])))", -limit, limit);
    
    beamDistr->SetParameter(0, 1.6);
    beamDistr->SetParameter(1, 282845);
    beamDistr->SetParameter(2, 2.40542);
    beamDistr->SetParameter(3, 1.0);
    
    beamDistr->SetParameter(4, 0.0164005);
    beamDistr->SetParameter(5, -3.17656);
    
    beamDistr->SetParameter(6, 1.55);
    beamDistr->SetParameter(7, 462240);
    beamDistr->SetParameter(8, 0.773682);    
    beamDistr->SetParameter(9, 0.0975392);
    beamDistr->SetParameter(10, 0.951919);
    
    return beamDistr;  
}

//vers 1 radius is gaussian angle is flat
void GammaSource::GetRandomMomentum(std::vector<double> &momentum, std::vector<double> &position)
{
	//double radius = radiusDistr->GetRandom();
	//double angle = angleDistr->Uniform(2.*TMath::Pi());
	//double y = radius*TMath::Cos(angle);
	//double z = radius*TMath::Sin(angle);
	
	double y = radiusDistr->GetRandom();
	double z = radiusDistr->GetRandom();
	double radius = TMath::Sqrt(y*y + z*z);
	double angle = angleDistr->Uniform(2.*TMath::Pi());
	y = radius*TMath::Cos(angle);
	z = radius*TMath::Sin(angle);
	//cosY = radiusDistr->GetRandom(); 
	//double angle = angleDistr->Uniform(2.*TMath::Pi());
	
	//GetRandom2 (Double_t &xrandom, Double_t &yrandom)
	
	position = {x0, y, z};	
	momentum = {sourceDistance*1000., y, z};
		
}

void GammaSource::MakeEnergyAndRadiusDistr(int energyIndex)
{
    double limit = 28;
    switch (energyIndex) 
    {
        case 0 :
             energy = 5.8;             
             radiusDistr = MakeFunc5dot8MeV("radiusDistr", limit);
             break;
        case 1 :
             energy = 8.3;
             radiusDistr = MakeFunc8dot3MeV("radiusDistr", limit);            
             break;
        case 2 :
             energy = 13;
             radiusDistr = MakeFunc13MeV("radiusDistr", limit);            
             break;
        case 3 :
             energy = 17;
             radiusDistr = MakeFunc17MeV("radiusDistr", limit);            
             break;
        default:
            const std::string msg = "GammaSource::MakeEnergyAndRadiusDistr - energy index out of range ";
            throw std::invalid_argument(msg);
            break;
    }
    MakeEnergyDistr(energy);
}

void GammaSource::MakeEnergyDistr(double energy)
{
    double sigmaE = 0.005*energy;
    double enMin = energy - 5.*sigmaE;
    if(enMin < 0)
        enMin = 0;
    double enMax = energy + 5.*sigmaE;
	energyDistr = new TF1("energyDistr", "TMath::Gaus(x,[0],[1],0)", enMin, enMax);
    energyDistr->SetParameters(energy, sigmaE);
}
