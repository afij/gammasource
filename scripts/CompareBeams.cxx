TH1F* GetPosHistogram(std::string filename);


void CompareBeams()
{
	TH1F* histo_5_8MeV = GetPosHistogram("out_5.8MeV.root");
    TH1F* histo_13MeV = GetPosHistogram("out_13MeV.root");
    TH1F* histo_17MeV = GetPosHistogram("out_17MeV.root"); 
    
    histo_17MeV->SetLineColor(kGreen);
    histo_17MeV->Draw();    
    histo_13MeV->SetLineColor(kRed);
    histo_13MeV->Draw("same");
    histo_5_8MeV->Draw("same");
}


TH1F* GetPosHistogram(std::string filename)
{

    TFile* f = new TFile(filename.c_str());
    TTree* t=(TTree*)f->Get("gammaBeam");
    //TOF
    double xMin = -20;
    double xMax = 20;
    double xNBins = 5.*(xMax-xMin);
    //QDC
    double yMin = -20;
    double yMax = 20;
    double yNBins = 5.*(yMax-yMin);

    TH1F* beamY = new TH1F("beamY","beamY",yNBins,yMin,yMax);    
    t->Draw("posY>>beamY");
    return beamY;
}


