
void PlotBeamY()
{
	//exp spectrum
    TFile* f = new TFile("out_17MeV.root");
    TTree* t=(TTree*)f->Get("gammaBeam");
    //TOF
    double xMin = -20;
    double xMax = 20;
    double xNBins = 5.*(xMax-xMin);
    //QDC
    double yMin = -20;
    double yMax = 20;
    double yNBins = 5.*(yMax-yMin);

    TH1F* beamY = new TH1F("beamY","beamY",yNBins,yMin,yMax);    
    t->Draw("posY>>beamY");
    
    beamY->Draw();
    TH1F* beamZ = new TH1F("beamZ","beamZ",yNBins,yMin,yMax);    
    t->Draw("posZ>>beamZ");
    beamZ->SetLineColor(kRed);
    
    beamY->Draw();
    beamZ->Draw("same");
}





