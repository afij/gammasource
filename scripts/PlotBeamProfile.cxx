
void PlotBeamProfile()
{
	//exp spectrum
    TFile* f = new TFile("out_17MeV.root");
    TTree* t=(TTree*)f->Get("gammaBeam");
    //TOF
    double xMin = -10;
    double xMax = 10;
    double xNBins = 5.*(xMax-xMin);
    //QDC
    double yMin = -10;
    double yMax = 10;
    double yNBins = 10.*(yMax-yMin);

    TH2F* beamYZ = new TH2F("beamYZ","beamYZ",xNBins,xMin,xMax,yNBins,yMin,yMax);
   //calibration scale 0.139625 
    
    t->Draw("posY:posZ>>beamYZ");
    //gPad->SetLogy();
    //gStyle->SetOptStat(0);
    //gStyle->SetPalette(kCMYK);
    gStyle->SetPalette(kCherry);
    //gStyle->InvertPalette();
    
    gPad->SetLogz();
        
    beamYZ->SetTitle("Beam Profile");
    beamYZ->GetXaxis()->SetTitle("Y (mm)");
    beamYZ->GetXaxis()->CenterTitle(true);
    beamYZ->GetXaxis()->SetLabelOffset(0.015);
    beamYZ->GetXaxis()->SetTitleOffset(1.40);
    beamYZ->GetYaxis()->SetTitle("Z (mm)");
    beamYZ->GetYaxis()->CenterTitle(true);
    beamYZ->GetYaxis()->SetLabelOffset(0.015);
    beamYZ->GetYaxis()->SetTitleOffset(1.40);
    
    beamYZ->Draw("colz2");

}





