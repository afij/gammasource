
void PlotBeamEnergy()
{
	//exp spectrum
    TFile* f = new TFile("out_13MeV.root");
    TTree* t=(TTree*)f->Get("gammaBeam");

    double yMin = 8;
    double yMax = 8.6;
    double yNBins = 100.*(yMax-yMin);

    TH1F* beamY = new TH1F("beamEn","beamEn",yNBins,yMin,yMax);
   //calibration scale 0.139625 
    
    t->Draw("energy>>beamEn");
    beamY->SetTitle("Beam Energy");
    beamY->GetXaxis()->SetTitle("Energy (MeV)");
    beamY->GetXaxis()->CenterTitle(true);
    beamY->GetXaxis()->SetLabelOffset(0.015);
    beamY->GetXaxis()->SetTitleOffset(1.40);
    beamY->GetYaxis()->SetTitle("Counts");
    beamY->GetYaxis()->CenterTitle(true);
    beamY->GetYaxis()->SetLabelOffset(0.015);
    beamY->GetYaxis()->SetTitleOffset(1.40);
    beamY->Draw();

}





